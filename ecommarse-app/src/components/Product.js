
import React from 'react';
import { Button, Card } from 'react-bootstrap';
import { CartState } from '../context/Context';
import Rating from './Rating';

const Product = ({ pro }) => {

  const { state:
    { cart },
    dispatch,
  } = CartState();

  return (

    <div className="products">
      <Card>
        <Card.Img variant="top" src={pro.image} alt={pro.name} />
        <Card.Body>
          <Card.Title>{pro.name}</Card.Title>
          <Card.Subtitle style={{ paddingBottom: 10 }}>
            <span>₹ {pro.price.split(".")[0]}</span>
            {pro.fastDelivery ? (
              <div>Fast Delivery</div>
            ) : (
              <div>4 days delivery</div>
            )}
            <Rating rating={pro.ratings} />
          </Card.Subtitle>
          {cart.some((p) => p.id === pro.id) ? (
            <Button
              variant="danger"
              onClick={() =>
                dispatch({
                  type: "REMOVE_FROM_CART",
                  payload: pro,
                })
              }
            >
              Remove from Cart
            </Button>
          ) : (
            <Button
              variant='warning text-white'
              onClick={() =>
                dispatch({
                  type: "ADD_TO_CART",
                  payload: pro,
                })
              }
              disabled={!pro.inStock}
            >
              {!pro.inStock ? "Out of Stock" : "Add to Cart"}
            </Button>
          )}
        </Card.Body>
      </Card>
    </div>
  )
}

export default Product