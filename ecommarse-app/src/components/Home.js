import React from 'react'
import { CartState } from '../context/Context';
import Product from './Product';
import './style.css';
import Filter from './Filter';

const Home = () => {
    const { state: { products },
        productState: { byStock, byFastDelivery, sort, byRating, searchQuery },
    } = CartState();

    // console.log(products)

    const transFormProduct = () => {

        let SortProducts = products;

        if (sort) {
            SortProducts = SortProducts.sort((a, b) =>
                sort === 'lowToHigh' ? a.price - b.price : b.price - a.price
            );
        }

        if (!byStock){
            SortProducts = SortProducts.filter((pro) => pro.inStock);

        }
        if (byFastDelivery){
            SortProducts = SortProducts.filter((pro) => pro.fastDelivery);

        }

        if (byRating){
            SortProducts = SortProducts.filter((pro) => pro.ratings >= byRating);

        }

        if(searchQuery){
            SortProducts = SortProducts.filter((pro) =>
            pro.name.toLowerCase().includes(searchQuery))
            &&(  SortProducts = SortProducts.filter((pro) => pro.fastDelivery));
            
            
            ;
        }
        


            return SortProducts;

    }
    return (
        <div className='home'>
            <Filter />
            <div className='productContainer'>
                {transFormProduct().map((pro) => {
                    return <Product pro={pro} key={pro.id} />
                })}
            </div>

        </div>
    )
}

export default Home