
import { Form, Button } from 'react-bootstrap';
import { CartState } from '../context/Context';
import Rating from './Rating';
import './style.css'

const Filter = () => {

    // const [rate, setRate] = useState(3);

    const { productState:
        {
            byStock,
            byFastDelivery,
            sort,
            byRating,


        },

        productDispatch
    } = CartState();

    console.log(byFastDelivery,
        byStock,
        byRating,
        sort,

    );

    return (
        <div className='filter'>
            <span className='title'>Filter a Product</span>
            <span>
                <Form.Check
                    inline
                    label="Ascending"
                    name="group1"
                    type="radio"
                    id={`inline-1`}

                    // onchange funch if its lowToHigh then cheack 
                    onChange={() =>
                        productDispatch({
                            type: 'SORT_BY_PRICE',
                            payload: 'lowToHigh',
                        })
                    }
                    // cheack lowToHigh weather its true or false
                    checked={sort === 'lowToHigh' ? true : false}
                />
            </span>
            <span>
                <Form.Check
                    inline
                    label="Descending"
                    name="group1"
                    type="radio"
                    id={`inline-2`}


                    // onchange funch if its highToLow then cheack 
                    onChange={() =>
                        productDispatch({
                            type: 'SORT_BY_PRICE',
                            payload: 'highToLow',
                        })
                    }
                    // cheack highToLow weather its true or false
                    checked={sort === 'highToLow' ? true : false}

                />
            </span>

            <span>
                <Form.Check
                    inline
                    label="Include Out of Stock"
                    name="group1"
                    type="checkbox"
                    id={`inline-3`}


                    // onchange funch FILTER_BY_STOCK to che if it includes 
                    onChange={() =>
                        productDispatch({
                            type: 'FILTER_BY_STOCK',

                        })
                    }
                    checked={byStock}



                />
            </span>
            <span>
                <Form.Check
                    inline
                    label="Fast Delivery Only"
                    name="group1"
                    type="checkbox"
                    id={`inline-4`}



                    onChange={() =>
                        productDispatch({
                            type: 'FILTER_BY_DELIVERY',

                        })
                    }
                    checked={byFastDelivery}
                />
            </span>
            <span>
                <label style={{ paddingRight: 10 }}> Rating</label>
                <Rating
                    rating={byRating}
                    onClick={(i) =>
                        productDispatch({
                            type: 'FILTER_BY_RATING',
                            payload: i + 1,
                        })
                    }

                    style={{ cursor: "pointer" }} />
            </span>
            <Button
                variant='warning'
                onClick={() =>
                    productDispatch({
                        type: 'CLEAR_FILTER',
                    })

                }


            >
                Clear Filters
            </Button>

        </div>
    )
}

export default Filter