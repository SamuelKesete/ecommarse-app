import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Image, ListGroup, Row } from 'react-bootstrap';
import { CartState } from '../context/Context';
import Rating from '../components/Rating'
import { AiFillDelete } from 'react-icons/ai';

const Cart = () => {
  const {
    state: { cart },
    dispatch

  } = CartState();

  const [total, setTotal] = useState();

  // calculate total

  useEffect(() => {
    setTotal(cart.reduce((acc, curr) => acc + Number(curr.price) * curr.qty, 0))

  }, [cart])

  return (
    <div className='home'>
      <div className='productContainer'>
        <ListGroup>
          {
            cart.map((pro) => (

              <ListGroup.Item key={pro.id}>
                <Row>
                  <Col md={2}>
                    <Image src={pro.image} alt={pro.name} fluid rounded />
                  </Col>
                  <Col md={2}>
                    <span>{pro.name}</span>
                  </Col>
                  <Col md={2}>
                    <span>{pro.price}</span>
                  </Col>
                  <Col md={2}>
                    <Rating rating={pro.ratings} />
                  </Col>
                  <Col md={2}>
                    <Form.Control as='select' value={pro.qty}
                    onChange={(e) =>
                    dispatch({
                      type: 'CHANGE_CART_QUY',
                      payload:{
                        id: pro.id,
                        qty: e.target.value,
                      },
                    })
                    }
                    
                    >
                      {[...Array(pro.inStock).keys()].map((x) => (
                        <option key={x + 1}>{x + 1}</option>
                      ))}
                    </Form.Control>
                  </Col>
                  <Col md={2}>
                    <Button
                    type='button'
                    variant='light'
                    onClick={()=>
                    dispatch({
                      type: 'REMOVE_FROM_CART',
                      payload: pro
                    })
                    }
                    >
                      <AiFillDelete fontSize='20px' className='text-danger'/>

                    </Button>
                  </Col>

                </Row>
              </ListGroup.Item>
            ))
          }
        </ListGroup>

      </div>
      <div className='filter summary'>
        <span className='title'>SubTotal ({cart.length}) items</span>
        <span style={{ fontWeight: 700, fontSize: 20 }}>Total: $ {total}</span>

        <Button className='bg-warning' type='button' disabled={cart.length === 0}>
          product to Cheackout

        </Button>

      </div>


    </div>
  )
}

export default Cart