# Getting Started with Create React App

This project created with bootstarap5 [Create React App]().

The purpose of this project is to create a shop cart Where the user has the opportunity to add products to cart and or remove the product from cart. 
The user also has the option of being able to filter the products by ascending, descending with including out of stock, fast delivery or if the product is high or low reting.  


### `Run app`

To run the app use `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\


### `npm installation`

npm install bootstrap
npm install faker
npm install useReducer




