import React from 'react';
import {
    Badge,
    Button,
    Container,
    Dropdown,
    FormControl,
    Nav,
    Navbar,

} from 'react-bootstrap';
import { AiFillDelete } from 'react-icons/ai';

import { FaShoppingCart } from 'react-icons/fa'
import { Link } from 'react-router-dom';
import { CartState } from '../context/Context';


const Header = () => {

    const { state:
        { cart },
        dispatch,
        productDispatch

    } = CartState();
    return (
        <Navbar className='bg-info' variant='dark' style={{ hight: 80 }}>
            <Container>
                <Navbar.Brand>
                    <Link to='/'>Shoping Cart</Link>
                </Navbar.Brand>
                <Navbar.Text className='search'>
                    <FormControl style={{ width: 300 }} placeholder='Search'
                        className='m-auto'

                        onChange={(e) => {
                            productDispatch({
                                type: 'FILTER_BY_SEARCH',
                                payload: e.target.value,
                            });
                        }}
                    />
                </Navbar.Text>
                <Nav>
                    <Dropdown >
                        <Dropdown.Toggle variant='warning'>
                            <FaShoppingCart color='white' fontSize='25px' />
                            <Badge className="text-white bg-warning">{cart.length}</Badge>
                        </Dropdown.Toggle>
                        <Dropdown.Menu style={{}}>


                            {
                                cart.length > 0 ? (
                                    <>
                                        {

                                            cart.map((pro) => (
                                                <span className='cartItem' key={pro.id}>
                                                    <img
                                                        src={pro.image}
                                                        className="cartItemImg"
                                                        alt={pro.naem}
                                                    />
                                                    <div className='cartItemDetail'>
                                                        <span>{pro.name}</span>
                                                        <span>${pro.price.split(".")[0]}</span>

                                                    </div>
                                                    <AiFillDelete
                                                        fontSize='20px'
                                                        style={{ cursor: "pointer" }}

                                                        onClick={() =>
                                                            dispatch({
                                                                type: "REMOVE_FROM_CART",
                                                                payload: pro
                                                            })}
                                                    />
                                                </span>

                                            ))}
                                        <Link to="/cart">
                                            <Button className='btn-warning' style={{ width: "95%", margin: "0 10px" }}>
                                                Go To cart
                                            </Button>
                                        </Link>
                                    </>
                                ) :
                                    (
                                        <span style={{ padding: 10 }}> Cart is empty!</span>
                                    )
                            }



                        </Dropdown.Menu>
                    </Dropdown>
                </Nav>
            </Container>


        </Navbar>

    )
}

export default Header